package com.companies.companiesapi.rest;

import com.companies.companiesapi.model.Company;
import com.companies.companiesapi.repository.CompanyRepository;
import com.companies.companiesapi.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
@CrossOrigin("*")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository; // külmkapp

    @Autowired
    private CompanyService companyService;

    @GetMapping("/hello/{name}")
    public String sayHello(@PathVariable("name") String x) {
        return "Hello, " + x + "!";
    }

    @GetMapping
    public List<Company> giveMeAllTheCompaniesYouEverHave() { // kelner
        // kelner peab köögist küsima ettevõtted ja need lauda tooma
        return companyRepository.fetchCompanies();
    }

    @GetMapping("/{id}")
    public Company giveMeOneSingleCompany(@PathVariable("id") int id) {
        return companyRepository.fetchCompany(id);
    }

    @PostMapping
    public void addOrModifyCompany(@RequestBody Company c) {
        companyService.updateCompany(c);
    }

    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable("id") int id) {
        companyService.deleteCompany(id);
    }
}
