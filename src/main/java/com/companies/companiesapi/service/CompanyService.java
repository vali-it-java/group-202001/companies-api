package com.companies.companiesapi.service;

import com.companies.companiesapi.model.Company;
import com.companies.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository; // külmkapp

    public void updateCompany(Company c) {
        Assert.notNull(c, "The company not found!");
        Assert.isTrue(c.getName() != null, "Company name not specified!");
        Assert.isTrue(c.getName().length() >= 2,
                "Company name must contain at least two characters!");
        Assert.isTrue(c.getLogo() != null, "Company logo not specified!");
        Assert.isTrue(c.getEmployees() > 0,
                "Company must have at least one employee!");

        if (c.getId() > 0) {
            // Muutmine
            companyRepository.updateCompany(c);
        } else {
            // Lisamine
            companyRepository.addCompany(c);
        }
    }

    public void deleteCompany(int id) {
        Assert.isTrue(id > 0, "Company ID not specified!");
        Assert.notNull(companyRepository.fetchCompany(id),
                "The specified company was not found!");
        companyRepository.removeCompany(id);
    }

}
